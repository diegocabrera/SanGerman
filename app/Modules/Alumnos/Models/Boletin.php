<?php

namespace App\Modules\Alumnos\Models;

use App\Modules\Base\Models\Modelo;

use App\Modules\Alumnos\Models\Grados;
use App\Modules\Alumnos\Models\Asignaturas;
use App\Modules\Alumnos\Models\Alumnos;

class Boletin extends Modelo
{
    protected $table = 'boletin';
    protected $fillable = ["grado_id","asignatura_id","notas","alumno_id"];
    protected $campos = [
        'asignatura_id' => [
            'type' => 'select',
            'label' => 'Asignatura',
            'placeholder' => '- Seleccione un Asignatura',
            'url' => 'Agrega una URL Aqui!'
        ],
        'notas' => [
            'type' => 'text',
            'label' => 'Notas',
            'placeholder' => 'Notas del Boletin'
        ],
        'alumno_id' => [
            'type' => 'select',
            'label' => 'Alumno',
            'placeholder' => '- Seleccione un Alumno',
            'url' => 'Agrega una URL Aqui!'
        ]
    ];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['alumno_id']['options'] = Alumnos::pluck('nombre', 'id');
        $this->campos['asignatura_id']['options'] = Asignaturas::pluck('nombre', 'id');
    }

    public function Grados()
	{
		return $this->belongsTo('App\Modules\Alumnos\Models\Grados');
	}
    public function Alumnos()
	{
		return $this->belongsTo('App\Modules\Alumnos\Models\Alumnos');
	}
    public function Asignaturas()
	{
		return $this->belongsTo('App\Modules\Alumnos\Models\Asignaturas');
	}


}
