<?php

namespace App\Modules\Alumnos\Models;

use App\Modules\Base\Models\Modelo;


use App\Modules\Alumnos\Models\Grados;
use App\Modules\Alumnos\Models\ProfesoresEspeciales;



class Asignaturas extends Modelo
{
    protected $table = 'asignaturas';
    protected $fillable = ["nombre","codigo","grado_id","profesor_especial_id"];
    protected $campos = [
    'nombre' => [
        'type' => 'text',
        'label' => 'Nombre',
        'placeholder' => 'Nombre del Asignaturas'
    ],
    'codigo' => [
        'type' => 'text',
        'label' => 'Codigo',
        'placeholder' => 'Codigo del Asignaturas'
    ],
    'grado_id' => [
        'type' => 'select',
        'label' => 'Grado',
        'placeholder' => '- Seleccione un Grado',
        'url' => 'Agrega una URL Aqui!'
    ],
    'profesor_especial_id' => [
        'type' => 'select',
        'label' => 'Profesor Especial',
        'placeholder' => '- Seleccione un Profesor Especial',
        'url' => 'Agrega una URL Aqui!'
    ]
];

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->campos['grado_id']['options'] = Grados::pluck('nivel', 'id');
        $this->campos['profesor_especial_id']['options'] = ProfesoresEspeciales::pluck('nombre', 'id');

    }


}
