var aplicacion, $form, tabla;
$(function() {
	aplicacion = new app('formulario', {
		'limpiar' : function(){
			tabla.ajax.reload();
		}
	});

	$form = aplicacion.form;

	tabla = datatable('#tabla', {
		ajax: $url + "datatable",
		columns: [
			{
				"data":"grado_id",
				"name":"grado_id"
			},
			{
				"data":"asignatura_id",
				"name":"asignatura_id"
			},
			{
				"data":"notas",
				"name":"boletion.notas"
			},
			{
				"data":"alumno_id",
				"name":"alumno_id"
			}
		]
	});

	$('#tabla').on("click", "tbody tr", function(){
		aplicacion.buscar(this.id);
	});
});
