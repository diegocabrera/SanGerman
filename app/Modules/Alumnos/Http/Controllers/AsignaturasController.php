<?php

namespace App\Modules\Alumnos\Http\Controllers;

//Controlador Padre
use App\Modules\Alumnos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Alumnos\Http\Requests\AsignaturasRequest;

//Modelos
use App\Modules\Alumnos\Models\Asignaturas;

class AsignaturasController extends Controller
{
    protected $titulo = 'Asignaturas';

    public $js = [
        'Asignaturas'
    ];
    
    public $css = [
        'Asignaturas'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('alumnos::Asignaturas', [
            'Asignaturas' => new Asignaturas()
        ]);
    }

    public function nuevo()
    {
        $Asignaturas = new Asignaturas();
        return $this->view('alumnos::Asignaturas', [
            'layouts' => 'base::layouts.popup',
            'Asignaturas' => $Asignaturas
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Asignaturas = Asignaturas::find($id);
        return $this->view('alumnos::Asignaturas', [
            'layouts' => 'base::layouts.popup',
            'Asignaturas' => $Asignaturas
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Asignaturas = Asignaturas::withTrashed()->find($id);
        } else {
            $Asignaturas = Asignaturas::find($id);
        }

        if ($Asignaturas) {
            return array_merge($Asignaturas->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(AsignaturasRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Asignaturas = $id == 0 ? new Asignaturas() : Asignaturas::find($id);

            $Asignaturas->fill($request->all());
            $Asignaturas->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Asignaturas->id,
            'texto' => $Asignaturas->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Asignaturas::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Asignaturas::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Asignaturas::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Asignaturas::select([
            'id', 'nombre', 'codigo', 'grado_id', 'profesor_especial_id', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}