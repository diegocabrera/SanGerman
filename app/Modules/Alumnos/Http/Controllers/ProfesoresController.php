<?php

namespace App\Modules\Alumnos\Http\Controllers;

//Controlador Padre
use App\Modules\Alumnos\Http\Controllers\Controller;

//Dependencias
use DB;
use App\Http\Requests\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Database\QueryException;

//Request
use App\Modules\Alumnos\Http\Requests\ProfesoresRequest;

//Modelos
use App\Modules\Alumnos\Models\Profesores;

class ProfesoresController extends Controller
{
    protected $titulo = 'Profesores';

    public $js = [
        'Profesores'
    ];
    
    public $css = [
        'Profesores'
    ];

    public $librerias = [
        'datatables'
    ];

    public function index()
    {
        return $this->view('alumnos::Profesores', [
            'Profesores' => new Profesores()
        ]);
    }

    public function nuevo()
    {
        $Profesores = new Profesores();
        return $this->view('alumnos::Profesores', [
            'layouts' => 'base::layouts.popup',
            'Profesores' => $Profesores
        ]);
    }

    public function cambiar(Request $request, $id = 0)
    {
        $Profesores = Profesores::find($id);
        return $this->view('alumnos::Profesores', [
            'layouts' => 'base::layouts.popup',
            'Profesores' => $Profesores
        ]);
    }

    public function buscar(Request $request, $id = 0)
    {
        if ($this->permisologia($this->ruta() . '/restaurar') || $this->permisologia($this->ruta() . '/destruir')) {
            $Profesores = Profesores::withTrashed()->find($id);
        } else {
            $Profesores = Profesores::find($id);
        }

        if ($Profesores) {
            return array_merge($Profesores->toArray(), [
                's' => 's',
                'msj' => trans('controller.buscar')
            ]);
        }

        return trans('controller.nobuscar');
    }

    public function guardar(ProfesoresRequest $request, $id = 0)
    {
        DB::beginTransaction();
        try{
            $Profesores = $id == 0 ? new Profesores() : Profesores::find($id);

            $Profesores->fill($request->all());
            $Profesores->save();
        } catch(QueryException $e) {
            DB::rollback();
            //return response()->json(['s' => 's', 'msj' => $e->getMessage()], 500);
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch(Exception $e) {
            DB::rollback();
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }
        DB::commit();

        return [
            'id'    => $Profesores->id,
            'texto' => $Profesores->nombre,
            's'     => 's',
            'msj'   => trans('controller.incluir')
        ];
    }

    public function eliminar(Request $request, $id = 0)
    {
        try{
            Profesores::destroy($id);
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.eliminar')];
    }

    public function restaurar(Request $request, $id = 0)
    {
        try {
            Profesores::withTrashed()->find($id)->restore();
        } catch (QueryException $e) {
           return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.restaurar')];
    }

    public function destruir(Request $request, $id = 0)
    {
        try {
            Profesores::withTrashed()->find($id)->forceDelete();
        } catch (QueryException $e) {
            return ['s' => 'n', 'msj' => $e->getMessage()];
        } catch (Exception $e) {
            return ['s' => 'n', 'msj' => $e->errorInfo[2]];
        }

        return ['s' => 's', 'msj' => trans('controller.destruir')];
    }

    public function datatable(Request $request)
    {
        $sql = Profesores::select([
            'id', 'nombre', 'dni', 'grado_id', 'deleted_at'
        ]);

        if ($request->verSoloEliminados == 'true') {
            $sql->onlyTrashed();
        } elseif ($request->verEliminados == 'true') {
            $sql->withTrashed();
        }

        return Datatables::of($sql)
            ->setRowId('id')
            ->setRowClass(function ($registro) {
                return is_null($registro->deleted_at) ? '' : 'bg-red-thunderbird bg-font-red-thunderbird';
            })
            ->make(true);
    }
}